from mcpi import minecraft
mc = minecraft.Minecraft.create()


bloque = 24  # piedra de arena
altura = 10
niveles = reversed(range(altura))

pos = mc.player.getTilePos()
x, y, z = pos.x + altura, pos.y, pos.z

for nivel in niveles:
    mc.setBlocks(x - nivel, y, z - nivel, x + nivel, y, z + nivel, bloque)
    y += 1