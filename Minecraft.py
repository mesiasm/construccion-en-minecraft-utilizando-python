from behave import step
from mcpi import minecraft
import time
mc = minecraft.Minecraft.create()
pos = mc.player.getTilePos()
sueloX = pos.x - 2
sueloY = pos.y - 1
sueloZ = pos.z - 2
anchura = 5
longitud = 5
bloque = 41


def creaPilar(x, y, z, altura):
    """Crea un pilar. Los argumentos son posición y altura del pilar"""
    bloqueEscalon = 156
    bloque = 155

    # Parte de arriba del pilar
    mc.setBlocks(x - 1, y + altura, z - 1, x + 1, y + altura, z + 1, bloque, 1)
    mc.setBlock(x - 1, y + altura - 1, z, bloqueEscalon, 12)
    mc.setBlock(x + 1, y + altura - 1, z, bloqueEscalon, 13)
    mc.setBlock(x, y + altura - 1, z + 1, bloqueEscalon, 15)
    mc.setBlock(x, y + altura - 1, z - 1, bloqueEscalon, 14)

    # Parte de la base del pilar
    mc.setBlocks(x - 1, y, z - 1, x + 1, y, z + 1, bloque, 1)
    mc.setBlock(x - 1, y + 1, z, bloqueEscalon, 0)
    mc.setBlock(x + 1, y + 1, z, bloqueEscalon, 1)
    mc.setBlock(x, y + 1, z + 1, bloqueEscalon, 3)
    mc.setBlock(x, y + 1, z - 1, bloqueEscalon, 2)

    # columna del pilar
    mc.setBlocks(x, y, z, x, y + altura, z, bloque, 2)

pos = mc.player.getTilePos()
x, y, z = pos.x , pos.y, pos.z

for desplazamientoX in range(0, 100, 5):
    creaPilar(x + desplazamientoX, y, z, 10)


"""Crea una pista de baile"""

mc.setBlocks(sueloX, sueloY, sueloZ,
             sueloX + anchura, sueloY, sueloZ + longitud, bloque)

while sueloX <= pos.x <= sueloX + anchura and sueloZ <= pos.z <= sueloZ + longitud:
    if bloque == 41:
        bloque = 57
    else:
        bloque = 41
    mc.setBlocks(sueloX, sueloY, sueloZ,
                 sueloX + anchura, sueloY, sueloZ + longitud, bloque)
    # obtén la posición del jugador
    pos = mc.player.getTilePos()
    # espera 0.5 segundos
    time.sleep(0.5)


"""Crea una piramide"""
bloque = 24  # piedra de arena
altura = 10
niveles = reversed(range(altura))

pos = mc.player.getTilePos()
x, y, z = pos.x + altura, pos.y, pos.z

for nivel in niveles:
    mc.setBlocks(x - nivel, y, z - nivel, x + nivel, y, z + nivel, bloque)
    y += 1

"""Suelta flores rojas durante recorrido del muñeco"""
while True:
    pos = mc.player.getPos()
    mc.setBlock(pos.x, pos.y, pos.z, 38)
    time.sleep(0.2)
    break

